AtomPlunkerView = require './atom-plunker-view'
{CompositeDisposable} = require 'atom'

module.exports = AtomPlunker =
  atomPlunkerView: null
  modalPanel: null
  subscriptions: null

  activate: (state) ->
    @atomPlunkerView = new AtomPlunkerView(state.atomPlunkerViewState)
    @modalPanel = atom.workspace.addModalPanel(item: @atomPlunkerView.getElement(), visible: false)

    # Events subscribed to in atom's system can be easily cleaned up with a CompositeDisposable
    @subscriptions = new CompositeDisposable

    # Register command that toggles this view
    @subscriptions.add atom.commands.add 'atom-workspace', 'atom-plunker:toggle': => @toggle()

  deactivate: ->
    @modalPanel.destroy()
    @subscriptions.dispose()
    @atomPlunkerView.destroy()

  serialize: ->
    atomPlunkerViewState: @atomPlunkerView.serialize()

  toggle: ->
    console.log 'AtomPlunker was toggled!'

    if @modalPanel.isVisible()
      @modalPanel.hide()
    else
      @modalPanel.show()
